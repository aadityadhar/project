from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import View
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, Template
from web.forms import LoginForm, RegisterForm
from django.contrib.auth import authenticate, login, logout
# Create your views here.


class Login(View):
    template = "login.html"

    def get(self, request):
        form = LoginForm()
        return render(request, self.template, {'login': form})

    def post(self, request):
        error = ""
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    error = "Your account is deactivated"
            else:
                error = "username and password does not match"
        errors = form.errors
        return render(request, self.template,{
            'Login': form, 'Login_error': error, 'Login_errors': errors
        })


class Register(View):
    template = "register.html"

    def get(self, request):
        form = RegisterForm()
        return render(request, self.template, {'register': form})

    def post(self, request):
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            form.cleaned_data.pop('password2')
            User.objects.create_user(**form.cleaned_data)
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/')
        errors = form.errors
        return render(request, self.template, {
            'Register': form,
            'register_errors': errors
        })


class Logoff(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')
