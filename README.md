# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the repository for the code to be deployed. It contains parser, scorer and mapper modules
* Version : 
Python==2.7.12
beautifulsoup4==4.5.1
bs4==0.0.1
Django==1.10
docx==0.2.4
ghostscript==0.4.1
html2text==2016.5.29
location==0.0.7
lxml==3.6.4
nltk==3.2.1
numpy==1.11.1
panda==0.3.1
pandas==0.18.1
pdfminer==20140328
Pillow==3.3.1
pymongo==3.3.0
pyPdf==1.13
PyPDF2==1.26.0
pypiwin32==219
python-dateutil==2.5.3
pytz==2016.6.1
requests==2.11.1
six==1.10.0
virtualenv==15.0.3

### How do I get set up? ###

* Install the set of packages specified under version above in a virtualenv.
* Clone this repository to the specified Virtualenv
* cd to the path where the manage.py file for this project is stored
* Run the command in the terminal "python manage.py runserver"

### Contribution guidelines ###

* The first page that'll open up when you go to the localhost(127.0.0.1:8000) is the login page.
* If you're an existing user, enter your details and click login. Or. Click on register now and fill out your details in the form before clicking SignUp.
* The next page will be the profile created for you with your details. Click on the button below Start Configuration to begin.
* The next page will be the Upload JD. You can select a JD from the local directory and submit it. The submit button will lead you to the next page with a form where you can enter the details missed out in the parsing of the JD. "The parsing module will run on the file uploaded on the click of Submit Button"
* Submitting the form will lead you to the Upload Resume. You can select Multiple Resumes and submit them. "The parsing module will run on the click of the submit button". "The Scoring Module will also run on the click of the submit button"
* The next page will show a Persona generated from giving score to the resumes and mapping them to create a preference flow. It will also give two buttons for uploading resumes from local directory and submit them for scoring and mapping.
* The next page will show a differentiated list of the candidates in the order of the TOP 5%, Next 10% and so on.
* The list here will have an email blast option that sends an invite email to all the candidates in that particular list.
* The names of the candidates in this list act as a link to their subsequent profile which includes their specified data from the Resume and the result of the Facebook Crawler.

### Who do I talk to? ###

* Repo owner or admin : Aaditya Dhar
* aaditya.dhar@gmail.com