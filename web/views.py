from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import View
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, Template
from web.forms import JobDescriptionForm, JobRoleForm
from django.contrib.auth.models import User
from web.models import JobDescription, Resume
from web.jobdescription import jd_parse


class Index(View):
    template = "index.html"

    def get(self, request):
        if request.user.is_authenticated():
            return render(request, self.template, {'user': request.user})
        return HttpResponseRedirect('/account/login/')


class UploadJobDescription(View):
    template = "jd.html"

    def get(self, request):
        if request.user.is_authenticated():
            form = JobDescriptionForm()
            return render(request, self.template, {'user': request.user, 'form':form})
        return HttpResponseRedirect('/account/login/')

    def post(self, request):
        if request.user.is_authenticated():
            form = JobDescriptionForm(request.POST, request.FILES)
            if form.is_valid():
                file = form.cleaned_data['jd']
                jd_object = JobDescription.objects.create(user=request.user, jd=file)
                jd_data = jd_parse(jd_object.jd.url)
                jd_object.jd_data = str(jd_data)
                jd_object.save()
                return HttpResponseRedirect('/jobrole/%s/' % (jd_object.id,))
            return render(request, self.template, {'user': request.user, 'form': form})
        return HttpResponseRedirect('/account/login/')


class JobRole(View):
    template = 'jobroleform.html'

    def get_object(self, model, id):
        return model.objects.get(pk=id)

    def get(self, request, *args, **kwargs):
        form = JobRoleForm()
        return render(request, self.template, {'user': request.user, 'form': form})

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            form = JobRoleForm(request.POST)
            if form.is_valid():
                jd_object = self.get_object(JobDescription, kwargs.get('jd_id'))
                jd_object.jd_data = str(form.cleaned_data)
                jd_object.save()
                return HttpResponseRedirect('/jobrolepersona/%s/' % (jd_object.id,))
            return render(request, self.template, {'user': request.user, 'form': form})
        return HttpResponseRedirect('/account/login/')


class JobRolePersona(View):
    template = 'job_role_persona.html'

    def get(self, request, *args, **kwargs):
        id = kwargs.get('jd_id')
        jd = JobDescription.objects.get(pk=id)
        return render(request, self.template, {'user': jd.user, 'id': id, 'jd': eval(jd.jd_data)})


class UploadResume(View):
    template = "upload_resume.html"

    def get_object(self, model, id):
        return model.objects.get(pk=id)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return render(request, self.template, {'user': request.user})
        return HttpResponseRedirect('/account/login/')

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            id = kwargs.get('jd_id')
            jd = self.get_object(JobDescription, id)
            for file in request.FILES.getlist('files'):
                resume_object = Resume.objects.create(jd=jd, resume=file)
                # score, resume_data = resume_parse(resume_object.jd)
                # resume_object.resume_data = str(resume_data)
                # resume_object.score = score
                # resume_object.save()
            return HttpResponseRedirect('/resumemap/%s/' % (id,))
        return HttpResponseRedirect('/account/login/')


class ResumeMap(View):
    template = "mappedresult.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            jd = JobDescription.objects.filter(jd=kwargs.get('jd_id'))
            resumes = Resume.objects.filter(jd=jd)
            return render(request, self.template, {'user': request.user, 'resumes': resumes})
        return HttpResponseRedirect('/account/login/')

