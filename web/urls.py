from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Index.as_view()),
    url(r'^jobdescription/$', views.UploadJobDescription.as_view()),
    url(r'^jobrole/(?P<jd_id>[0-9]+)/$', views.JobRole.as_view()),
    url(r'^jobrolepersona/(?P<jd_id>[0-9]+)/$', views.JobRolePersona.as_view()),
    url(r'^resume/(?P<jd_id>[0-9]+)/$', views.UploadResume.as_view()),
    url(r'^resumemap/(?P<jd_id>[0-9]+)/$', views.ResumeMap.as_view()),
]