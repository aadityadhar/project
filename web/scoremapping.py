# import nltk
# s = "4+ years hands-on Java, J2EE development (JSP, servlet, Struts, JDBC, taglib, JSTL, JBoss, ATG Dynamo), 3+ years Perl design and development experience - perl data structures and references, CPAN, modules, and packages, 2+ years database experience (SQL, DDL/DML, stored procedures, data modeling). Sybase experience preferred, 2+ years experience with Open Source tools (Apache, JBoss, Tomcat, Hibernate, Ant, Struts, Spring, JUnit, etc.), 3+ years UNIX experience (ksh, bash, shell scripts)"
# token = nltk.word_tokenize(s)
# print token
import os
import re
import json
from pprint import pprint

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_jd_weighted(file_name="/home/aadityadhar/Desktop/JD/1.json"):
    extra = "No specification"
    weighted = dict()
    weighted['job role + experience'] = 0.234375
    weighted['skills'] = 0.203125
    weighted['college + marks'] = 0.171875
    weighted['location'] = 0.140625
    weighted['challenge'] = 0.109375
    weighted['key phrase'] = 0.078125
    weighted['related experience'] = 0.046875
    weighted['language'] = 0.015625

    max_signal = 10

    with open(file_name, 'r') as f:
        jd_data = json.loads(f.read())
    for key in jd_data:
        jd_data[key.lower()] = jd_data.pop(key)
    jd_parameters = jd_data.keys()
    # decay in weighted if not specified
    if ("experience" and "job role" not in jd_parameters) or (extra in jd_data['experience'] and jd_data['job role']):
        curr_signal = 0
        delta_w = weighted['job role + experience'] - 0.02(weighted['job role + experience']) * (
        max_signal - curr_signal) / 10

    if ("college" and "marks" not in jd_parameters) or (extra in jd_data['college'] and jd_data['marks']):
        curr_signal = 0
        delta_w = weighted['college + marks'] - 0.02(weighted['college + marks']) * (max_signal - curr_signal) / 10

    if ("location" not in jd_parameters) or (extra in jd_data['location']):
        curr_signal = 0
        delta_w['location'] = weighted['location'] - 0.02(weighted['location']) * (max_signal - curr_signal) / 10

    if ("challenge" not in jd_parameters) or (extra in jd_data['challenge']):
        curr_signal = 0
        delta_w['challenge'] = weighted['challenge'] - 0.02(weighted['challenge']) * (max_signal - curr_signal) / 10

    if ("key phrase" not in jd_parameters) or (extra in jd_data['key phrase']):
        curr_signal = 0
        delta_w['key phrase'] = weighted['key phrase'] - 0.02(weighted['key phrase']) * (max_signal - curr_signal) / 10

    if ("related experience" not in jd_parameters) or (extra in jd_data['related experience']):
        curr_signal = 0
        delta_w['related experience'] = weighted['related experience'] - 0.02(weighted['related experience']) * (
        max_signal - curr_signal) / 10

    if ("language" not in jd_parameters) or (extra in jd_data['language']):
        curr_signal = 0
        delta_w['language'] = weighted['language'] - 0.02(weighted['language']) * (max_signal - curr_signal) / 10

    return jd_data, weighted


def convert_txt_to_json(file_name="/home/aadityadhar/Desktop/Resume/2.txt"):
    education = '''Higher Secondary School  :
  School Name  :  Delhi Public School
  School Location  :  Yamuna Nagar,Haryana
  Board  :  No match in the file as per the rules
  Specialization  :  No match in the file as per the rules
  CGPA  :  9.2
  Passout Year  :  2010
Senior Secondary School  :
  School Name  :  D.A.V. Centenary Public School
  School Location  :  Simbla (Barara)
  Board  :  No match in the file as per the rules
  Specialization  :  No match in the file as per the rules
  Percentage  :  83%
  Passout Year  :  2012
Graduation College  :
  College Name  :  Jai Prakash Mukand Lal Innovative Engineering And Technology Institute
  College Location  :  Radaur(Yamuna Nagar)
  University  :  No match as per the rules specified
  Course  :  B.Tech.
  Specialization  :  Computer Engineering
  Passout Year  :  Pursuing'''
    pg_education = '''Post Graduation College  :
  College Name  :  No match in the file as per the rules
 College Location  :  No match in the file as per the rules
  University  :  No match in the file as per the rules
  Course  :  No match in the file as per the rules
  Specialization  :  No match in the file as per the rules
  Percentage\GPA  :  No match in the file as per the rules
  Passout Year  :  No match in the file as per the rules'''
    experience = '''Company Name  :  No match in the file as per the rules
  Company Location  :  No match in the file as per the rules
  Job Role  :  No match in the file as per the rules
  Time  :  No match in the file as per the rules
  Description  :  No match in the file as per the rules
  Company Name  :  Hartron Approved Workstation
  Company Location  :  Ambala Cantt.
  Job Role  :  No match in the file as per the rules
  Time  :  6 Weeks
  Description  :  No match in the file as per the rules  +
  Company Name  :  Allsoft Solutions
  Company Location  :  Mohali, Punjab
  Job Role  :  No match in the file as per the rules
  Time  :  No match in the file as per the rules
  Description  :  No match in the file as per the rules'''

    project = '''Project Name  :  No match in the file as per the rules
  Company Name  :  No match in the file as per the rules
  Job Role  :  No match in the file as per the rules
  Description  :  No match in the file as per the rules
  Time  :  No match in the file as per the rules
  Technology Used  :  No match in the file as per the rules
  Project Name  :  No match in the file as per the rules
  Company Name  :  No match in the file as per the rules
  Job Role  :  No match in the file as per the rules
  Description  :  No match in the file as per the rules
  Time  :  No match in the file as per the rules
  Technology Used  :  No match in the file as per the rules'''
    with open(file_name, 'r') as f:
        resume_data = f.read().split('\n')
    resume_data_map = dict()
    for line in resume_data:
        temp = line.split(':')
        try:
            resume_data_map[temp[0].strip()] = temp[1].strip()
        except IndexError:
            pass
    education = education.split('\n')
    temp = ""
    for i in xrange(len(education)):
        if i % 7 == 0:
            resume_data_map[education[i].split(':')[0].strip()] = dict()
            temp = education[i].split(':')[0].strip()
        else:
            curr_data = education[i].split(':')
            resume_data_map[temp].update({curr_data[0].strip(): curr_data[1].strip()})
    pg_education = pg_education.split('\n')
    for i in xrange(len(pg_education)):
        if i % 7 == 0:
            resume_data_map[pg_education[i].split(':')[0].strip()] = dict()
            temp = pg_education[i].split(':')[0].strip()
        else:
            curr_data = pg_education[i].split(':')
            resume_data_map[temp].update({curr_data[0].strip(): curr_data[1].strip()})

    experience = experience.split('\n')
    resume_data_map['experience'] = []
    for i in xrange(len(experience)):
        if i % 5 == 0:
            resume_data_map['experience'] += [{}]
        curr_data = experience[i].split(':')
        resume_data_map['experience'][i / 5].update({curr_data[0].strip(): curr_data[1].strip()})

    project = project.split('\n')
    resume_data_map['project'] = []
    for i in xrange(len(project)):
        if i % 6 == 0:
            resume_data_map['project'] += [{}]
        curr_data = project[i].split(':')
        resume_data_map['project'][i / 6].update({curr_data[0].strip(): curr_data[1].strip()})
    file_name = file_name.split('.')
    file_name[1] = 'json'
    file_name = '.'.join(file_name)
    with open(file_name, 'w') as f:
        f.write(json.dumps(resume_data_map))


def get_initial_value_of_skill(jd_skills):
    total_score = 10.0
    diff = -0.2
    l = len(jd_skills)
    max_value = total_score / l - ((l - 1) * diff) / 2
    return max_value


def get_initial_value_of_experience(jd_experience):
    total_score = 10.0
    diff = -0.2
    l = len(jd_skills)
    max_value = total_score / l - ((l - 1) * diff) / 2
    return max_value


def get_initial_value_of_college(jd_education):
    total_score = 10.0
    diff = -0.2
    l = len(jd_education)
    max_value = total_score / l - ((l - 1) * diff) / 2
    return max_value


def get_initial_value_of_jobrole(jd_jobrole):
    total_score = 10.0
    diff = -0.2
    l = len(jd_jobrole)
    max_value = total_score / l - ((l - 1) * diff) / 2
    return max_value


def get_initial_value_of_location(jd_location):
    total_score = 10.0
    diff = -0.2
    l = len(jd_location)
    max_value = total_score / l - ((l - 1) * diff) / 2
    return max_value


def map_resume_score(file_name='/home/aadityadhar/Desktop/Resume/2.json'):
    resume_score = 0.0


jd_data, weight = get_jd_weighted('/home/aadityadhar/Desktop/JD/1.json')
with open(file_name, 'r') as f:
    resume_data = json.loads(f.read())
for key in resume_data:
    resume_data[key.lower()] = resume_data.pop(key)
resume_parameters = resume_data.keys()
# pprint(resume_parameters)
pprint(resume_score)

# calculating skills score
resume_skills = resume_data['softwares']
resume_skills = re.sub(r"(?<!\w)[()](?!\w)", "", resume_skills).split(' + ')
jd_data['skills'] = re.sub(r"(?<!\w)[()](?!\w)", "", jd_data['skills']).split(',')
jd_skills = jd_data['skills']
max_value = get_initial_value_of_skill(jd_skills)
count = 0
for skill in jd_skills:
    for resume_skill in resume_skills:
        if skill in resume_skill or resume_skill in skill:
            resume_score += max_value - .2 * count
            break
    count += 1
pprint(resume_data.keys())

# calculating experience score
resume_experience = resume_data['time']
resume_experience = re.sub(r"(?<!\w)[()](?!\w)", "", resume_experience).split(' + ')
jd_data['experience'] = re.sub(r"(?<!\w)[()](?!\w)", "", jd_data['experience']).split(',')
jd_experience = jd_data['experience']
max_value = get_initial_value_of_experience(jd_experience)
count = 0
for experience in jd_experience:
    for resume_experience in resume_experience:
        if experience in resume_experience or resume_experience in experience:
            resume_score += max_value - .2 * count
            break
    count += 1

# calculating college score
resume_college = resume_data['Education' & 'Post Graduate Education']
resume_college = re.sub(r"(?<!\w)[()](?!\w)", "", resume_college).split(' + ')
jd_data['Academic Qualification'] = re.sub(r"(?<!\w)[()](?!\w)", "", jd_data['Academic Qualification']).split(',')
jd_education = jd_data['Academic Qualification']
max_value = get_initial_value_of_college(jd_college)
count = 0
for college in jd_college:
    for resume_college in resume_college:
        if college in resume_college or resume_college in college:
            resume_score += max_value - .2 * count
            break
    count += 1

# calculating jobrole score
resume_jobrole = resume_data['job role']
resume_jobrole = re.sub(r"(?<!\w)[()](?!\w)", "", resume_jobrole).split(' + ')
jd_data['jobrole'] = re.sub(r"(?<!\w)[()](?!\w)", "", jd_data['jobrole']).split(',')
jd_jobrole = jd_data['jobrole']
max_value = get_initial_value_of_jobrole(jd_jobrole)
count = 0
for jobrole in jd_jobrole:
    for resume_jobrole in resume_jobrole:
        if jobrole in resume_jobrole or resume_jobrole in jobrole:
            resume_score += max_value - .2 * count
            break
    count += 1

# calculating location score
resume_location = resume_data['Primary Location' & 'Secondary Location']
resume_location = re.sub(r"(?<!\w)[()](?!\w)", "", resume_location).split(' + ')
jd_data['Location'] = re.sub(r"(?<!\w)[()](?!\w)", "", jd_data['Location']).split(',')
jd_location = jd_data['Location']
max_value = get_initial_value_of_location(jd_location)
count = 0
for location in jd_location:
    for resume_location in resume_location:
        if location in resume_location or resume_location in location:
            resume_score += max_value - .2 * count
            break
    count += 1

pprint(resume_score)
# get_jd_score()
convert_txt_to_json()
map_resume_score()