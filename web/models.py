from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class FacebookInfo(models.Model):
    email = models.EmailField()
    image = models.URLField()
    detail = models.TextField()


class JobDescription(models.Model):
    user = models.ForeignKey(User)
    jd = models.FileField(upload_to='JobDescription/')
    jd_data = models.TextField(null=True, blank=True)


class Resume(models.Model):
    jd = models.ForeignKey(JobDescription)
    resume = models.FileField(upload_to='resumes/')
    resume_data = models.TextField(null=True, blank=True)
    score = models.FloatField(null=True, blank=True)
