import csv
import functools
import glob
import logging
import os
import re
import argparse
from os import chdir, getcwd, listdir, path
#import PyPDF2
import pandas
import sys
from time import strftime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def check_path(file_name=None):
    file_name = BASE_DIR + '/' + file_name
    try:
        with open(file_name, 'r') as f:
            return f.read()
    except:
        return None


def check_location():
    file = '/home/aadityadhar/Desktop/LocationMatrix.txt'
    with open(file, 'r') as fi:
        data = fi.read().split('\n')
        location = data


def list_files(path=os.getcwd()):
    file_list = list()
    for (dirpath, dirnames, filenames) in os.walk(path):
        os.path.join(dirpath)
        filenames = [os.path.join(dirpath, filename) for filename in filenames]
        file_list.extend(filenames)
    return file_list


def check_phonenumber(string_to_search):
    try:
        regex = re.compile(r"\(?"
                           r"(\d{3})?"
                           r"\)?"
                           r"[\s\.-]{0,2}?"
                           r"(\d{3})"
                           r"[\s\.-]{0,2}"
                           r"(\d{4})",
                           re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.groups()
            result = "-".join(result)
        return result
    except Exception, e:
        logging.error('Issue parsing phone number: ' + str(string_to_search) + str(e))
        return None


def check_fathersname(string_to_search):
    try:
        regex = re.compile(r"\bSon Of\b|\bS/O\b | \bFather's Name\b[A-Z]+ [A-Z ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error("Issue parsing father's name: " + str(string_to_search) + str(e))
        return None


def check_email(string_to_search):
    try:
        regex = re.compile(r"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing email id: ' + str(string_to_search) + str(e))
        return None


def check_currentaddress(string_to_search):
    try:
        regex = re.compile(r" \bCurrent Address\b[0-9]+ [a-z0-9,\.# ]", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing current address: ' + str(string_to_search) + str(e))
        return None


def check_permanentaddress(string_to_search):
    try:
        regex = re.compile(r"\bPermanent Address\b[0-9]+ [a-z0-9,\.# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing permanent address: ' + str(string_to_search) + str(e))
        return None


class parse(object):
    """docstring for parse"""
    def __init__(self, arg):
        super(parse, self).__init__()
        self.arg = arg

    def check_name(self, string_to_search):
        for string in string_to_search:
            if string.IGNORECASE != "Curriculum Vitae"|"CV"|"Resume"|"Snapshot"|"Biodata":
                try:
                    names = []
                    for each in self.name:
                        if each[0] not in self.headings:
                            each = each[1].replace('Name',"")
                            if each[0] not in string.letters:
                                each = each[1:]
                            names.append(each.strip())
                        else:
                            index = self.headings[self.headings.index(each[0])+1]
                            names.append("\n".join(self.content[each[0]+1:index]))
                    if len(names) == 1:
                        return names[0]
                    else:
                        return names
                except Exception, e:
                    logging.error('Issue parsing name:' + str(string_to_search) + str(e))
                    return None

    def get_keywords(self):
        try:
            self.keywords = []
            for element in self.words_dict:
                if self.words_dict[element][1] != 'PERSON' and self.words_dict[element][1] != 'O':
                    self.keywords.append(self.words_dict[element][0])
        except Exception, e:
            logging.error('Issue parsing keyword:' + str(string_to_search) + str(e)) 
            return None   


def check_dob(string_to_search):
    try:
        regex = re.compile[(r"(\d{2})?"
                           r"\)?"
                           r"[\s\.-]{0,2}?"
                           r"(\d{2})"
                           r"[\s\.-]{0,2}"
                           r"(\d{4}|{2})",
                           re.IGNORECASE)]
        result = re.search(regex, string_to_search)
        if result:
            result = result.groups()
            result = "/".join(result)
        return result
    except Exception, e:
        logging.error('Issue parsing dob: ' + str(string_to_search) + str(e))
        return None


def check_phonenumber2(string_to_search):
    try:
        regex = re.compile(r"\(?"
                           r"(\d{3})?"
                           r"\)?"
                           r"[\s\.-]{0,2}?"
                           r"(\d{3})"
                           r"[\s\.-]{0,2}"
                           r"(\d{4})",
                           re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.groups()
            result = "-".join(result)
        return result
    except Exception, e:
        logging.error('Issue parsing phone number2: ' + str(string_to_search) + str(e))
        return None


def check_email2(string_to_search):
    try:
        regex = re.compile(r"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing email id2: ' + str(string_to_search) + str(e))
        return None


def check_school(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b|\b10\b|\b12\b|\bHSC\b|\bSSC\b[A-Z]+ [A-Z0-9,\# ]+\bSchool\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing school: ' + str(string_to_search) + str(e))
        return None


def check_schoollocation(string_to_search):
    try:
        loc4 = str(check_school(string_to_search))
        loc4 = loc4.strip(location)
        return loc4
    except Exception, e:
        logging.error('Issue parsing schoollocation: ' + str(string_to_search) + str(e))
        return None


def check_specialization(string_to_search):
    try:
        regex = re.compile(r"\bStudied\b|\bPursued\b|\bPursuing\b|\bSubjects\b[A-Z]+ [A-Z0-9,\# ]+\bSchool\b|\bCollege\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing specialization: ' + str(string_to_search) + str(e))
        return None


def check_passout(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[0-9,\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing passout: ' + str(string_to_search) + str(e))
        return None


def check_board(string_to_search):
    try:
        regex = re.compile(r"\bBoard\b[A-Z]+ \b\n\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing board: ' + str(string_to_search) + str(e))
        return None


def check_score(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[0-9,\# ]+\b%\b|\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing weighted: ' + str(string_to_search) + str(e))
        return None


def check_schoollocation(string_to_search):
    try:
        loc4 = str(check_school(string_to_search)).result
        result = loc4.strip(location)
        if result:
            reult = result.group()
        return result
    except Exception, e:

        logging.error('Issue parsing schoollocation: ' + str(string_to_search) + str(e))
        return None

def check_college(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[A-Z]+ [A-Z0-9,\# ]+\bCollege\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing college: ' + str(string_to_search) + str(e))
        return None


def check_collegelocation(string_to_search):
    try:
        str1 = str(check_college(string_to_search)).result
        str1 = str1.strip(location)
        result = str1
        if result:
            result = result.group()
            return result
    except Exception, e:
        logging.error('Issue parsing college location: ' + str(string_to_search) + str(e))
        return None


def check_university(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[A-Z]+ [A-Z0-9,\# ]+\bUniversity\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing university: ' + str(string_to_search) + str(e))
        return None


def check_branch(string_to_search):
    try:
        regex = re.compile(r"\bStudied\b|\bPursued\b|\bPursuing\b[A-Z]+ [A-Z0-9,\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing branch: ' + str(string_to_search) + str(e))
        return None


def check_specialization(string_to_search):
    try:
        regex = re.compile(r"\bStudied\b|\bPursued\b|\bPursuing\b[A-Z]+ [A-Z0-9,\# ]+\bCollege\b|\bUniversity\b", re.IGNORECASE)
        result = re.search(regex, string_to_search) - str(check_branch(string_to_search))
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing specialization: ' + str(string_to_search) + str(e))
        return None


def check_passout(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[0-9,\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing passout: ' + str(string_to_search) + str(e))
        return None


def check_score(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[0-9,\# ]+\b%\b|\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            reult = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing weighted: ' + str(string_to_search) + str(e))
        return None


def check_primarylocation(string_to_search):
    try:
        for loc1 in str(check_currentaddress(string_to_search)).result:
            loc1 = loc.strip(location)
            result = loc1
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing primary location: ' + str(string_to_search) + str(e))
        return None


def check_secondarylocation(string_to_search):
    try:
        for loc2 in str(check_permanentaddress(string_to_search)).result:
            loc2 = loc.strip(location)
            result = loc2
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing secondary location: ' + str(string_to_search) + str(e))
        return None


def check_company(string_to_search):
    try:
        regex = re.compile(r"\bWork at\b|\bEmployed at\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing company: ' + str(string_to_search) + str(e))
        return None


def check_prevcompany(string_to_search):
    try:
        regex = re.compile(r"\bWorked at\b|\bPreviously Employed at\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing prevcompany: ' + str(string_to_search) + str(e))
        return None

    # def check_companylocation(string_to_search):
    #     try:
    #         loc3 = check_company().result
    #         loc3 = loc3.strip(location)
    #         if result:
    #             result = result.group()
    #         return result
    #     except Exception, e:
    #         logging.error('Issue parsing companylocation: ' + str(string_to_search) + str(e))
    #         return None


# def check_prevcompanylocation(string_to_search):
#     try:
#         loc3 = check_prevcompany().result
#         loc3 = loc3.strip(location)
#         if result:
#             result = result.group()
#         return result
#     except Exception, e:
#         logging.error('Issue parsing prevcompanylocation: ' + str(string_to_search) + str(e))
#         return None


def check_jobrole(string_to_search):
    try:
        regex = re.compile(r"\bWork as\b|\bEmployed as\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Isssue parsing jobrole: ' + str(string_to_search) + str(e))
        return None


def check_prevjobrole(string_to_search):
    try:
        regex = re.compile(r"\bWorked as\b|\bEmployed as\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Isssue parsing prevjobrole: ' + str(string_to_search) + str(e))
        return None


def check_time(string_to_search):
    try:
        regex = re.compile(r"\bWorked for\b|\bEmployed for\b[0-9]+ [A-Z,/\# ]+\bYears\b|\bMonths\b|\bWeeks\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Isssue parsing time: ' + str(string_to_search) + str(e))
        return None


def check_totalexp(string_to_search):
    try:
        time = str(check_time(string_to_search)).result
        result = time
        return result
    except Exception, e:
        logging.error('Issue parsing totalexp: ' + str(string_to_search) + str(e))
        return None


def check_project(string_to_search):
    try:
        regex = re.compile(r"\bWorked on\b|\bProject\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing project: ' + str(string_to_search) + str(e))
        return None


def check_description(string_to_search):
    try:
        regex = re.compile(r"\bRole and Responsibility\b|\bDescription\b[A-Z]+ [A-Z0-9,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing description: ' + str(string_to_search) + str(e))
        return None


def check_language(string_to_search):
    try:
        regex = re.compile(r"\bSpeak\b|\bRead\b|\bWrite\b[A-Z ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Isssue parsing language: ' + str(string_to_search) + str(e))
        return None


def check_softwares(string_to_search):
    try:
        regex = re.compile(r"\bWorked On\b|\bProefficent in\b|\bTechnical Skills\b[A-Z]+ [A-Z0-9,./\+# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Isssue parsing softwares: ' + str(string_to_search) + str(e))
        return None


def check_interpersonalskills(string_to_search):
    try:
        regex = re.compile(r"\bInterpersonal Skills\b|\bPeople Skills\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing interpersonalskills: ' + str(string_to_search) +str(e))
        return None


def check_softskills(string_to_search):
    try:
        regex = re.compile(r"\bOther Skills\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing softskills: ' + str(string_to_search) +str(e))
        return None


def check_maritalstatus(string_to_search):
    try:
        regex = re.compile(r"\bMarital Status\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing maritalstatus: ' + str(string_to_search) +str(e))
        return None


def check_Nationality(string_to_search):
    try:
        regex = re.compile(r"\bNationality\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing Nationality: ' + str(string_to_search) +str(e))
        return None


def check_refrences(string_to_search):
    try:
        regex = re.compile(r"\bRefrences\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing refrences: ' + str(string_to_search) +str(e))
        return None


def check_extracurricularactivities(string_to_search):
    try:
        regex = re.compile(r"\bExtra Curricular Activities\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing extracurricularactivities: ' + str(string_to_search) +str(e))
        return None


def check_achievements(string_to_search):
    try:
        regex = re.compile(r"\bAchievements\b|\bAwards\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing Achievements: ' + str(string_to_search) +str(e))
        return None


def check_hobbies(string_to_search):
    try:
        regex = re.compile(r"\bHobbies\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing Hobbies: ' + str(string_to_search) +str(e))
        return None


def check_mothersname(string_to_search):
    try:
        regex = re.compile(r"\bMother's Name\b[A-Z]+ [A-Z0-9,./\#+ ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        logging.error('Issue parsing mothersname: ' + str(string_to_search) +str(e))
        return None


# def check_softskills(string_to_search):
#     try:
#         regex = re.compile(r"\bOther Skills\b[A-Z]+ [A-Z0-9,./\#+- ]+\b.\b", re.IGNORECASE)
#         result = re.search(regex, string_to_search)
#         if result:
#             result = result.group()
#         return result
#     except Exception, e:
#         logging.error('Issue parsing skills: ' + str(string_to_search) +str(e))
#         return None


def term_count(string_to_search, term="me"):
    try:
        regex = re.compile(term, re.IGNORECASE)
        result = re.findall(regex, string_to_search)
        return len(result)
    except Exception, e:
        logging.error('Issue parsing term: ' + str(term)+' from string: ' + str(
            string_to_search) + ': ' + str(e))
        return 0


def term_match(string_to_search, term="me"):
    try:
        regex = re.compile(term, re.IGNORECASE)
        result = re.findall(regex, string_to_search)
        return result[0]
    except Exception, e:
        logging.error('Issue parsing term: ' + str(term) + ' from string: ' +
                      str(string_to_search) +': ' + str(e))
        return None


# def build_dataframe(path):
#     logging.info('Searching path: ' + str(path))
#     path_glob = os.path.join(path, '*.pdf')
#     file_list =[pdf_file for pdf_file in glob.glob(path_glob)]

#     logging.info('Iterating through file_list: ' + str(file_list))
#     df = pd.DataFrame()

#     # Get file information
#     df["file_name"] = file_list
#     df["html"] = df["file_name"].apply(convert_pdf_to_txt)

#     # Scrape contact information
#     df["phone_number"] = df["html"].apply(check_phonenumber)
#     # df["area_code"] = df["phone_number"].apply(functools.partial(term_match, term=r"\d{3}"))
#     df["email"] = df["html"].apply(check_email)
#     # df["email_domain"] = df["email"].apply(functools.partial(term_match, term=r"@(.+)"))
#     df["address"] = df["html"].apply(check_address)
#     # df["linkedin"] = df["html"].apply(functools.partial(term_count, term=r"linkedin"))
#     # df["github"] = df["html"].apply(functools.partial(term_count, term=r"github"))

#     # Pull a few other interesting features
#     df["num_words"] = df["html"].apply(lambda x: len(x.split()))
#     df["phd"] = df["html"].apply(functools.partial(term_count, term=r"ph.?d.?"))

#     # And skills
#     # df["java_count"] = df["html"].apply(functools.partial(term_count, term=r"java"))
#     # df["python_count"] = df["html"].apply(functools.partial(term_count, term=r"python"))
#     # df["R_count"] = df["html"].apply(functools.partial(term_count, term=r" R[ ,]"))
#     # df["latex_count"] = df["html"].apply(functools.partial(term_count, term=r"latex"))
#     # df["stata_count"] = df["html"].apply(functools.partial(term_count, term=r"stata"))
#     # df["CS_count"] = df["html"].apply(functools.partial(term_count, term=r"computer science"))
#     # df["mysql_count"] = df["html"].apply(functools.partial(term_count, term=r"mysql"))
#     # df["ms_office"] = df["html"].apply(functools.partial(term_count, term=r"microsoft office"))
#     # df["analytics"] = df["html"].apply(functools.partial(term_count, term=r"analytics"))
#     return df


# def main():
#     parser = argparse.ArgumentParser(
#         description='Script to parse PDF resumes, and create a csv file containing contact info '
#                     'and required fields')
#     parser.add_argument('--data_path', help='Path to folder containing PDF resumes.',
#                         required=True)
#     parser.add_argument('--output_path', help='Path to place output .csv file',
#                         default='resumes_output.csv')

#     args = parser.parse_args()

#     # Setup logger
#     logging.basicConfig(level=logging.INFO)
#     logging.info('Begin Main')

#     # Create dataframe
#     df = build_dataframe(args.data_path)

#     # Output to CSV
#     df.to_csv(args.output_path)
#     logging.info('End Main')


def resume_parse(resume_path):
    string_to_search = check_path(resume_path)
    resume_data = dict()
    resume_data['phonenumber'] = check_phonenumber(string_to_search)
    resume_data['fathersname'] = check_fathersname(string_to_search)
    resume_data['email'] = check_email(string_to_search)
    resume_data['currentaddress'] = check_currentaddress(string_to_search)
    resume_data['permanentaddress'] = check_permanentaddress(string_to_search)
    resume_data['email2'] = check_email2(string_to_search)
    resume_data['school'] = check_school(string_to_search)
    resume_data['schoollocation'] = check_schoollocation(string_to_search)
    resume_data['specialization'] = check_specialization(string_to_search)
    resume_data['passout'] = check_passout(string_to_search)
    resume_data['board'] = check_board(string_to_search)
    resume_data['score'] = check_score(string_to_search)
    resume_data['college'] = check_college(string_to_search)
    resume_data['collegelocation'] = check_collegelocation(string_to_search)
    resume_data['university'] = check_university(string_to_search)
    resume_data['branch'] = check_branch(string_to_search)
    resume_data['specialization'] = check_specialization(string_to_search)
    resume_data['passout'] = check_passout(string_to_search)
    resume_data['score'] = check_score(string_to_search)
    resume_data['primarylocation'] = check_primarylocation(string_to_search)
    resume_data['secondarylocation'] = check_secondarylocation(string_to_search)
    resume_data['company'] = check_company(string_to_search)
    resume_data['prevcompany'] = check_prevcompany(string_to_search)
    resume_data['jobrole'] = check_jobrole(string_to_search)
    resume_data['prevjobrole'] = check_prevjobrole(string_to_search)
    resume_data['time'] = check_time(string_to_search)
    resume_data['totalexp'] = check_totalexp(string_to_search)
    resume_data['project'] = check_project(string_to_search)
    resume_data['description'] = check_description(string_to_search)
    resume_data['language'] = check_language(string_to_search)
    resume_data['softwares'] = check_softwares(string_to_search)
    resume_data['interpersonalskills'] = check_interpersonalskills(string_to_search)
    resume_data['softskills'] = check_softskills(string_to_search)
    resume_data['maritalstatus'] = check_maritalstatus(string_to_search)
    resume_data['Nationality'] = check_Nationality(string_to_search)
    resume_data['extracurricularactivities'] = check_extracurricularactivities(string_to_search)
    resume_data['achievements'] = check_achievements(string_to_search)
    resume_data['hobbies'] = check_hobbies(string_to_search)
    resume_data['mothersname'] = check_mothersname(string_to_search)

    return resume_data

print resume_parse('Resume/resume1.txt')