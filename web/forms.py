from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):
    username = forms.CharField(required = True)
    password = forms.CharField(required = True)


class RegisterForm(forms.ModelForm):
    password2 = forms.CharField(required = True)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ['username', 'email','password','password2']

    def clean(self):
        cleaned_data = super(RegisterForm,self).clean()
        password = cleaned_data.get("password")
        password2 = cleaned_data.get('password2')
        email = cleaned_data.get("email")

        if email == "":
            raise forms.ValidationError(_("Fill the highlighted field"))

        if (password and password2 and password2 == password) :
            return cleaned_data
        else:
            raise forms.ValidationError(_("password does not match."))

    # def mail_send(self):
    #   send_mail.delay(self.cleaned_data['email'])


class JobDescriptionForm(forms.Form):
    jd = forms.FileField()


class JobRoleForm(forms.Form):
    job_role = forms.CharField()
    location = forms.CharField()
    salary = forms.CharField()
    qualification = forms.CharField()
    experience = forms.CharField()
    language = forms.CharField()
    college = forms.CharField()
    skill = forms.CharField()
    contact = forms.CharField()
    description = forms.CharField()
