import os
from pprint import pprint
import re

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def check_path(file_name=None):
    file_name = BASE_DIR + '/' + file_name
    try:
        with open(file_name, 'r') as f:
            return f.read()
    except:
        return None


def check_jobrole(string_to_search):
    try:
        regex = re.compile(r"\bJob Role\b|\bRole\b[A-Z]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_skills(string_to_search):
    try:
        regex = re.compile(r"\bWorked On\b|\bProefficent in\b|\bTechnical Skills\b[A-Z]+ [A-Z0-9,\# ]+\b.\b",
                           re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_college(string_to_search):
    try:
        regex = re.compile(r"\bStudied at\b[A-Z]+ [A-Z0-9,\# ]+\bCollege\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_language(string_to_search):
    try:
        regex = re.compile(r"\bSpeak\b|\bRead\b|\bWrite\b[A-Z ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_location(string_to_search):
    try:
        regex = re.compile(r" \bLocation\b[0-9]+ [a-z0-9,\.# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_experience(string_to_search):
    try:
        regex = re.compile(r"\bExperience\b|\bFresher\b[0-9]+ [A-Z,/\# ]+\bYears\b|\bMonths\b|\bWeeks\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_academicqualification(string_to_search):
    try:
        regex = re.compile(r"\bQualification\b[A-Z,/\# ]+\b.\b", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_salary(string_to_search):
    try:
        regex = re.compile(r"\bSalary\b|\bPay Package\b|\bCompensation\b|\bOffered\b[0-9]+ [A-Z,/\# ]+\bThousand\b|\bLakhs\b|\bCrores\b",
            re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_email(string_to_search):
    try:
        regex = re.compile(r"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_phonenumber(string_to_search):
    try:
        regex = re.compile(r"\(?"
                           r"(\d{3})?"
                           r"\)?"
                           r"[\s\.-]{0,2}?"
                           r"(\d{3})"
                           r"[\s\.-]{0,2}"
                           r"(\d{4})",
                           re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.groups()
            result = "-".join(result)
        return result
    except Exception, e:
        return ""


def check_contactdetails(string_to_search):
    try:
        result = str(check_email(string_to_search)) + " , " + str(check_phonenumber(string_to_search))
        return result
    except Exception, e:
        return ""


def check_description(string_to_search):
    try:
        result = check_jobrole(string_to_search)
        result += check_location(string_to_search)
        result += check_salary(string_to_search)
        result += check_academicqualification(string_to_search)
        result += check_experience(string_to_search)
        result += check_language(string_to_search)
        result += check_college(string_to_search)
        result += check_skills(string_to_search)
        result += check_contactdetails(string_to_search)
        return result
    except Exception, e:
        return ""


def jd_parse(jd_path):
    string_to_search = check_path(jd_path)
    jd_data = dict()
    jd_data['job_role'] = check_jobrole(string_to_search)
    jd_data['location'] = check_location(string_to_search)
    jd_data['salary'] = check_salary(string_to_search)
    jd_data['qualification'] = check_academicqualification(string_to_search)
    jd_data['experience'] = check_experience(string_to_search)
    jd_data['language'] = check_language(string_to_search)
    jd_data['college'] = check_college(string_to_search)
    jd_data['skills'] = check_skills(string_to_search)
    jd_data['contact'] = check_contactdetails(string_to_search)
    jd_data['description'] = check_description(string_to_search)
    return jd_data

pprint(jd_parse('JobDescription/4.txt'))
