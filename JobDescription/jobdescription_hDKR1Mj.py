import os
from pprint import pprint
import re

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def check_path(file_name=None):
    file_name = BASE_DIR + '/' + file_name
    try:
        with open(file_name, 'r') as f:
            return f.read()
    except:
        return None


def check_jobrole(string_to_search):
    try:
        string_to_search.startswith("Job Role | Role")
        string_to_search.endswith("\n")
        regex = re.compile(r"[A-Z]+ [A-Z0-9,/ ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_skills(string_to_search):
    try:
        string_to_search.startswith("Keyskills | Technology | Technologies")
        string_to_search.endswith("\n")
        regex = re.compile(r"[A-Z]+ [A-Z0-9/\,# ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_college(string_to_search):
    try:
        string_to_search.startswith("Qualification | Academic Qualification | Education | UG | PG | Doctorate")
        string_to_search.endswith("\n")
        regex = re.compile(r"[A-Z]+ [A-Z0-9,\# ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_language(string_to_search):
    try:
        string_to_search.startswith("Languages | Can Speak")
        string_to_search.endswith("\n")
        regex = re.compile(r"[A-Z]+ [A-Z0-9,/ ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_location(string_to_search):
    try:
        string_to_search.startswith("Location | Place | Posting at")
        string_to_search.endswith("\n")
        regex = re.compile(r"[A-Z]+ [a-z0-9,\.# ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_experience(string_to_search):
    try:
        string_to_search.startswith("Experience | Desired Profile")
        string_to_search.endswith("\n")
        regex = re.compile(r"[[A-Z]+ [a-z0-9,\.# ]+", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_benefits(string_to_search):
    try:
        string_to_search.startswith("Salary | Pay | Package | Compensation | Benefits")
        string_to_search.endswith("\n")
        regex = re.compile(r"[0-9]+ [A-Z,/\# ]+",
            re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_email(string_to_search):
    try:
        regex = re.compile(r"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}", re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.group()
        return result
    except Exception, e:
        return ""


def check_phonenumber(string_to_search):
    try:
        regex = re.compile(r"\(?"
                           r"(\d{3})?"
                           r"\)?"
                           r"[\s\.-]{0,2}?"
                           r"(\d{3})"
                           r"[\s\.-]{0,2}"
                           r"(\d{4})",
                           re.IGNORECASE)
        result = re.search(regex, string_to_search)
        if result:
            result = result.groups()
            result = "-".join(result)
        return result
    except Exception, e:
        return ""


def check_contactdetails(string_to_search):
    try:
        result = str(check_email(string_to_search)) +  " " + str(check_phonenumber(string_to_search))
        return result
    except Exception, e:
        return ""


def check_description(string_to_search):
    try:
        result = check_jobrole(string_to_search)
        result += check_location(string_to_search)
        result += check_salary(string_to_search)
        result += check_academicqualification(string_to_search)
        result += check_experience(string_to_search)
        result += check_language(string_to_search)
        result += check_college(string_to_search)
        result += check_skills(string_to_search)
        result += check_contactdetails(string_to_search)
        return string_to_search - result
    except Exception, e:
        return ""


def jd_parse(jd_path):
    string_to_search = check_path(jd_path)
    jd_data = dict()
    jd_data['job_role'] = check_jobrole(string_to_search)
    jd_data['location'] = check_location(string_to_search)
    jd_data['benefits'] = check_benefits(string_to_search)
    jd_data['experience'] = check_experience(string_to_search)
    jd_data['language'] = check_language(string_to_search)
    jd_data['college'] = check_college(string_to_search)
    jd_data['skills'] = check_skills(string_to_search)
    jd_data['contact'] = check_contactdetails(string_to_search)
    jd_data['description'] = check_description(string_to_search)
    return jd_data

pprint(jd_parse('/home/aadityadhar/Downloads/1.txt'))
